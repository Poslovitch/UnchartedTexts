extends CharacterBody2D
class_name Paper

@export var folio: Folio
var show_recto: bool = true
## This Paper is being dragged
var dragged: bool = false
var mouse_in: bool = false
var new_position: Vector2 = Vector2()
var dragging_distance: float = 0.0
var dragging_direction: Vector2 = Vector2()

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed() and mouse_in:
			print("Dragged")
			dragged = true
			dragging_distance = position.distance_to(get_viewport().get_mouse_position())
			dragging_direction = (get_viewport().get_mouse_position() - position).normalized()
			new_position = get_viewport().get_mouse_position() - dragging_distance * dragging_direction
		else:
			print("Dragging stopped")
			dragged = false
	elif event is InputEventMouseMotion:
		if dragged:
			new_position = get_viewport().get_mouse_position() - dragging_distance * dragging_direction

# Called when the node enters the scene tree for the first time.
func _ready():
	_update_shown_text()

func _physics_process(delta):
	if dragged:
		velocity = (new_position - position) * 1000 * delta
		move_and_slide()

func _update_shown_text() -> void:
	if show_recto:
		$Text.text = folio.recto_content
	else:
		$Text.text = folio.verso_content

func _on_flip_button_pressed():
	show_recto = !show_recto
	_update_shown_text()

func _on_mouse_entered():
	if not dragged:
		print("mouse in")
		mouse_in = true

func _on_mouse_exited():
	if not dragged:
		print("mouse out")
		mouse_in = false
