@tool
extends Resource
class_name Text

@export var name: String = "Unnamed"
@export var author: String = ""
@export_range(1600, 1900) var publication_date = 1900
@export var pages: Array[Folio] = []

enum TextType {DICTIONARY, NOVEL, POEM}
@export var type: TextType
