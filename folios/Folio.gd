@tool
extends Resource
class_name Folio

@export_group("Content")
## Content key for the recto of this folio.
@export_multiline var recto_content: String = ""
@export_multiline var verso_content: String = ""

@export_group("Key Points")

